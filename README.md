## Mark Nuzzo's README

Hello everyone, I'm Mark Nuzzo. I am the Backend Engineering Manager who supports the Pipeline Authoring team (Verify stage) here at GitLab. I'm hoping that this README will give everyone a chance to to know me better.

### About me

I live just outside of Chicago, Illinois with my wife, 3 children and puppy (Reyna). I love to run on my treadmill, go shopping, playing games with my kids as well as playing fantasy football every fall during the NFL season.

I typically work a standard 8am to 5pm CT work day with times where I move my hours around due to other conflicts.  At times, I start my day earlier or to offset working later have some gaps in the middle of my day too.  I try to accept as many Donut and coffee chats as possible to chat with team members from other areas of the company.  I welcome openness as well as areas of improvement discussions as that provides a gateway to how I can be better to support everyone which is why I align closest to our Transparency and Collaboration values.

### What am I working on

I recently started a list of weekly priorities to track what my focuses are each week.  I write up async issue updates (internal) about what we're working on in Verify:Pipeline Authoring.

### Best way to reach me

Slack: Feel free to tag me in a slack thread (or even for visibility as a cc), but for more urgency or a quick response, you can also DM me.
GitLab issues: please mention @marknuzzo - otherwise I may miss your comment.
Email (including Google Doc notifications): I do check email, but it's sometimes best to get into my GitLab todos :)
