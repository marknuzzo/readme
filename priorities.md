## Weekly Priorities

This list represents my focus that I have as goals for this upcoming week that I want to accomplish.  It will be updated weekly.

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.

Y - Completed  
N - Not Completed

### 2024-10-07 to 2024-10-11
- Updating technical roadmap
- Check-in on OKRs
- Wrap-up open follow-ups before PTO on Oct. 11 

### Ongoing Goals
- Reviewing sev2 SUS Impacting [issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=devops%3A%3Averify&label_name%5B%5D=SUS%3A%3AImpacting&label_name%5B%5D=severity%3A%3A2&label_name%5B%5D=group%3A%3Apipeline%20authoring&first_page_size=20) to be priortized 

### 2024-09-30 to 2024-10-04
- Reviewing needs weight board for rollover and prioritization **Y**
- Technical Roadmap review **Y**

### 2024-09-23 to 2024-09-27
- Reviewing backlog to prioritize other areas of impact for bigger customers **Y**
- Prep for monthly backlog cleanup meeting **Y**

### 2024-09-16 to 2024-09-20
- Data analysis related to technical roadmap priorities **Y**
- Reviewing Verify Technical Debt board for PA to apply severity or relabel **Y**
- Follow-ups on CI Steps next week plans after sync **Y**

### 2024-09-09 to 2024-09-13
- Reviewing top labels for technical roadmap priorities **Y**
- Check in on error budget trends and next work **Y**
- F&F Day on 2024-09-13 

### 2024-09-02 to 2024-09-06
- Reviewing error budget trends and follow-up with team **Y**
- Updating OKRs progress for Q3 **Y**
- Reviewing backlog for upcoming milestone themes per engineering technical strategy **Y**

### 2024-08-26 to 2024-08-30
- Reviewing tech debt [board](https://gitlab.com/groups/gitlab-org/-/boards/1438885?label_name[]=devops%3A%3Averify&label_name[]=type%3A%3Amaintenance&label_name[]=group%3A%3Apipeline%20authoring) and compare priorities with Q3/Q4 technical strategy. **Y**
- Reviewing 7d trends on error budget **Y**

### 2024-08-19 to 2024-08-23
- Prep for CI Steps/CI Catalog bi-weekly sync **Y**
- Reviewing current technical debt priorities for needs weight week consideration **Y**
- Career Development sync prep work **Y**

### 2024-08-12 to 2024-08-16
- Tenant Scale meeting to discuss any Core Dev upstram blocking work for Cells 1.0 **Y**
- Milestone retro sync **Y**
- Career Development Syncs with team members **Y**

### 2024-08-05 to 2024-08-09
- CI Section ThinkBIG discussion **Y**
- Reviewing board for milestone carryover planning **Y**
- Oncall M-Th **Y**
- Reviewing current upstream blocker statuses for Core Dev with Cells **Y**

### 2024-07-29 to 2024-08-02
- check-in on DB sharding prioritization for Cells **Y**
- Follow-up on group::respond discussion for work needing owners **Y**
- Reviewing `17.4` focus with Dov before socializing to team **Y**

### 2024-07-22 to 2024-07-26
- Prep for PA Backlog cleanup **Y**
- Prep for focus on themes in needs weight **Y**
- Sending out Engagement Survey follow-up form **Y**

### 2024-07-15 to 2024-07-19
- Prep for Community Office Hours Monthly **Y**
- Milestone Team Sync **Y**
- Getting co-create template merged **Y**
- Socializing future Hackathon cadence to core dev/Ops **Y**

### 2024-07-08 to 2024-07-12
- Fixing Google Workspace Integrations **Y**
- CI Steps/CI Catalog alignment sync prep **Y**
- Applying deliverable labels for `17.3` work **Y**

### 2024-07-01 to 2024-07-05
- Prepping `17.3` goals for Engineering **Y**
- Fixing Google Workspace Integrations **N**
- Holiday and PTO later in week.

### 2024-06-24 to 2024-06-28
- PTO 

### 2024-06-17 to 2024-06-21
- AI Hackathon **Y**
- Community Office Hours Monthly **Y**
- CI Catalog GA Retropspective Sync **Y**
- Milestone Retrospective Sync **Y**

### 2024-06-10 to 2024-06-14
- Design discussion on components visibility **Y**
- `17.1` check-in and rollover **Y**
- IGP **Y**

### 2024-06-03 to 2024-06-07
- Prep for customer call for CI Catalog/Components feedback **Y**
- Borrow request transitional work **Y**
- Cells 1.0 core development check-in on statuses across group **Y**

### 2024-05-27 to 2024-05-31
- Prep for needs weight **Y**
- Career Syncs prep with team members **Y**
- PA Backlog clean up **Y**

### 2024-05-20 to 2024-05-24
- Reviewing error budget downward trend to see if pipeline creation is affecting it **Y**
- Career Dev Syncs prep **Y**
- Prep for CI/CD Catalog Community Office Hours **Y**

### 2024-05-13 to 2024-05-17
- CI Steps/CI Catalog alignment bi-weekly sync **Y**
- Cells 1.0 weekly check-in with groups **Y**
- `17.0` milestone retro sync **Y**
- Reviewing epic structure/organization post-GA **Y**

### 2024-05-06 to 2024-05-10
- Components and Steps Study Sync **Y**
- Cells 1.0 check-in **Y**
- Add CI Steps OKRs for `Q2` **Y**

### 2024-04-29 to 2024-05-03
- Enter Q2 OKRs **Y**
- PA Backlog cleanup **Y**
- Prep for Post-GA Brainstorming Session **Y**
- Prep for Milestone Retro Sync **Y**

### 2024-04-22 to 2024-04-26
- OKR drafting **Y**
- Career Development check-ins **Y**
- Review Cells Fastboot notes and team follow-ups **Y**

### 2024-04-15 to 2024-04-19
- OKR drafting **N** (Still being worked on)
- Group coaching: Elevated Applied **Y**
- Brainstorming session for ThinkBig/ThinkSmall **N** (postponed)
- FY25Q1 Team Survey Prep and Review **Y**

### 2024-04-08 to 2024-04-12
- Q2 OKR prep **Y**
- Customer call for CI Catalog **Y**
- Quarterly career sync preps **Y**
- Follow ups with data on Tableau integration for CI Catalog weekly statuses.**Y**

### 2024-04-01 to 2024-04-05
- Quarterly career development syncs **Y**
- Customer call for CI Catalog **Y**
- Task Force sync **Y**

### 2024-03-25 to 2024-03-29
- Monthly CI/CD Catalog Community Office Hours **Y**
- Team Sync Retro for `16.10` **Y**
- Design Sprint Completion **Y**
- Bi-monthly Backlog cleanup **Y**

### 2024-03-18 to 2024-03-22
- Follow-ups on post-GA priorities and socializing everywhere needed. **Y**
- Design Sprint kickoff support **Y**
- Monthly CI/CD Catalog Community Office Hours **N** (delayed to March 26)
- Team Sync Retro for `16.10` **N** (Due to timing conflict, delayed to March 28)

### 2024-03-11 to 2024-03-15
- Summit 2024

### 2024-03-04 to 2024-03-08
- Give comp reviews for FY'24 **Y**
- Tableau work for reports **Y**
- Review needs weight board **Y**

### 2024-02-26 to 2024-03-01
- Wrap-up Cells 1.0 testing **Y**
- Follow-up with Dov on needs weight focus **Y**
- Organizing team focus beyond `17.0` for CI Catalog efforts **Y**

### 2024-02-19 to 2024-02-23
- Community Office Hours for CI Catalog **Y**
- Finish Cells 1.0 testing for PA **N**
- Next steps for Release/Publish process 2.0 **Y**
- Holiday on 2024-02-19 and PTO 2024-02-23 

### 2024-02-12 to 2024-02-16
- Triage-ops weekly report for gitlab.com/components contributions **Y**
- Cells 1.0 testing for PA **Y**
- Release/Publish process 2.0 sync **Y**

### 2024-02-05 to 2024-02-09
- Tableau POC **Y**
- Continue with Cells 1.0 workflows for PA **Y**
- 16.10 deliverable labeling **Y**

### 2024-01-29 to 2024-02-02
- Finalize OKRs for Q1 **Y**
- Testing Cells 1.0 workflows for PA **Y**
- Reviewing error budget with FF rollout **Y** 

### 2024-01-22 to 2024-01-26
- CI Section ThinkBIG **Y**
- Reviews **Y**
- Q1 OKR prep **Y**

### 2024-01-15 to 2024-01-19
- Holiday 2024-01-15
- Start yearly reviews **Y** 
- Organizing Go-To-GA Epic with deliverables and dates **Y**

### 2024-01-08 to 2024-01-12
- Finish talent assessment yearly reviews set up **Y**
- Prep for ThinkBig **Y**
- Review quick win list for community competition **Y**
- F&F Day on 2024-01-12 

### 2024-01-01 to 2024-01-05
- Reviewing this month's community competition focus **Y**
- Check-in on status for templates->componnents migration **Y**
- Setting up talent assessment yearly reviews **N**
- Check-in on needs weight board **Y**

### 2023-12-18 to 2023-12-22 
- FCL issue prioritization and summarizing progress. **Y**
- January competition weekly check-in with DevRel and Contributor Success **Y**
- Creating issues for January competition **Y**

### 2023-12-11 to 2023-12-15
- `16.8` planning prep **Y**
- Meeting to discuss January competition component strategy with DevRel/Contrib Success **Y**
- Rajendra onboarding **Y** (Ongoing)

### 2023-12-04 to 2023-12-08
- Promo planning prep **Y**
- Prep for final leadership coaching session **Y** 
- CI Catalog Beta epic close out **Y**
- Prepping for Community Competition prep meeting **Y**

### 2023-11-27 to 2023-12-01
- Finalizing needs weight board order for `16.8` **Y**
- Discuss Hackathon/CC competition next steps for CI Catalog **Y**
- CI Catalog Ops Key Project check-in **Y**
- Monitor Beta progress for CI Catalog **Y**
- On holiday Thurs/Friday and F&F Day Monday

### 2023-11-20 to 2023-11-24
- Discussing strategy for building components via Hackathon **Y**
- Reviewing options for handbook pages for components ownership **Y**
- Monitor Beta progress for CI Catalog 
- On holiday Thurs/Friday and F&F Day Monday

### 2023-11-13 to 2023-11-17
- Sharing feedback from KubeCon with team **Y**
- Create reports for RTTM for PA in Sisense  **Y**
- `16.7` organization for CI Catalog for Beta release. **Y**

### 2023-11-06 to 2023-11-10
- Weekly check-ins with Dov/Sunjung and Fabio **Y**
- KubeCon Conference **Y**
- Weekly wrap-up on CI Catalog progress on Friday afternoon (my time) **Y**

### 2023-10-30 to 2023-11-03
- Finalize Q4 OKRs **Y**
- CI Catalog team weekly sync - walk the board, discuss blockers or areas of concern **Y**
- 2 BE interviews for PA role. **Y**
- Complete Manager self-evaluations in Workday. **Y**
- Leadership/Coaching session. **Y**

### 2023-10-23 to 2023-10-27
- On-call 2023-10-23 **Y**
- Q4 OKRs review  **Y**
- FY24 Calibration drafts and review **Y**
- CI Catalog team weekly sync - walk the board, discuss blockers or areas of concern **Y**
- 2 BE interviews for PA role. **Y**

### 2023-10-16 to 2023-10-20
- Finalizing `16.6` milestone plan and applying `deliverable` labels **Y**
- Self-evaluation in Workday **Y**
- Drafting Q4 OKRs **Y**
- CI Catalog team weekly sync - discuss blockers or areas of concern for resolution **Y**
- On-call 2023-10-20 **Y**

### 2023-10-09 to 2023-10-13
- Reviewing current themes for `16.6` and `16.5` potential carryover **Y**
- Working on updates to talent assessment calibration drafts **Y**
- Reviewing resumes for open role **Y**
- Needs weight board rollover maintenance/close out. **Y**
- Holiday on 2023-10-09

### 2023-10-02 to 2023-10-06
- Working on updates to talent assessment calibration drafts **Y**
- CI Catalog organization for inputs to GA and components to Beta issues for `16.6` **Y**
- Ops Showcase Reviews for September **Y**
- Reviewing resumes for open role **Y**
- F&F Day on 2023-10-06

### 2023-09-25 to 2023-09-29
- Completing first draft of talent assessment calibration **Y**
- Prep for leadership coaching meeting **Y**
- Determine next steps for candidates being interviewed this week for open role in PA **Y**
- Ops sub-dept review of CI Catalog **Y**

### 2023-09-18 to 2023-09-22 
- Interviews and entering feedback for BE role and PE PM **Y**
- Firming up MR for tentative delivery dates for PA projects **Y**
- Reviewing bug and tech debt reports for prioritization **Y**
- F&F for September on 2023-09-22 

### 2023-09-12 to 2023-09-15
- `16.5` board prep based on priorities **Y**
- Interview prep for next week for open BE role and review resumes **Y**
- CI Steps timeline issue creation and discussion **Y**
- OKR check-ins on CI Catalog progress **Y**

### 2023-09-04 to 2023-09-08
- Record Elevate cohort project and certification exam on Thursday **Y**
- Interview prep and interview for candidate for open BE role **Y**
- Review and rollover needs weight board **Y**
- **NOTE:** Holiday on 2023-09-04 and PTO on 2023-09-08

### 2023-08-28 to 2023-09-01
- Reviewing resumes for open BE position **Y**
- Needs weight board prep for `16.5` **Y**
- Elevate training project work, module training and leadership training **Y**
- PTO on 2023-09-01

### 2023-08-21 to 2023-08-25
- Updating CI Catalog project plan and moving issues to post-GA epic **Y**
- Reviewing needs weight candidates for next week's `16.5` planning **Y**
- Customer meeting on 2023-08-22 to discuss CI Catalog **Y** 
- F&F Day on Friday 2023-08-25

### 2023-08-14 to 2023-08-18
- Working on team project for Elevate training **Y**
- Adding deliverables to `16.4` and `16.3` rollover **Y**
- Check-in on CI Steps and monthly release process status **Y**

### 2023-08-07 to 2023-08-11
- Module 5 Elevate training **Y**
- Prioritizing `sev2` list for future milestones **Y**
- Integrating MRs into epic format for headway reporting **N** (Waiting on https://github.com/NARKOZ/gitlab/pull/675 to merge first) 
- Reviewing `at risk` issues in `16.3` and balancing with `16.4` list **Y**
- Discuss kickoff issue with Sara **N** (Waiting on approvals in https://gitlab.com/gitlab-headcount/r-and-d-headcount/-/issues/379)

### 2023-07-31 to 2023-08-04
- Merge new reporting format for Headway project reporting **Y**
- Module 4 Elevate training **Y**
- Reviewing `16.4` planning issue for updated priorities **Y**
- Finish setting up OKRs **Y**

### 2023-07-24 to 2023-07-28 
- Complete restructuring of Headway project reporting **Y**
- Leadership coaching training **Y**
- Organizing CI Catalog epics with next milestone issues **Y**
- Getting needs weight board preppred for next week **Y**

### 2023-07-17 to 2023-07-21
- Career Dev Sync planning for team members **Y**
- Investigation into next iteration of automated MR/Issue milestone reporting for projects **Y**
- Elevate project work and leadership coaching tasks for competition review and thinking strategically **Y**

### 2023-07-10 to 2023-07-14
- Team Offsite for Q2 from 2023-07-11 to 2023-07-13 
- Completing Headway fork for CI Catalog MR reporting **Y**
- Elevate Module 4 training on 2023-07-13 **Y**

### 2023-07-03 to 2023-07-07
- F&F and Holiday to begin week
- Review upcoming release posts for merging **Y**
- Debugging JSON file population for SSOT for releases in the future **Y**

### 2023-06-26 to 2023-06-30 
- Prepping needs weight board for `16.3` **Y**
- Elevate Module 3: Step 3 group coaching **Y**
- Reviewing Sisense dashboard for high priority support issues in Zendesk **Y**

### 2023-06-19 to 2023-06-23 
- Coaching training **Y**
- Fixing needs weight PA calendar content **Y**
- Reviewing CI Catalog MVC launch remaining tasks **Y**

### 2023-06-12 to 2023-06-16 
- Elevate Module 3 training **Y**
- Organizing issues based on `16.2` priorities **Y**
- Discuss CI Events next steps **Y**

### 2023-05-29 to 2023-06-02 
- Ops Hackathon Showcase **Y**
- Finish On-call publishing procedures **Y**
- Think Big Sync **Y**
- CI Early Adopters Review and Feedback **Y**

### 2023-05-22 to 2023-05-26 
- Support for Ops hackathon week! **Y**
- Reviewing upcoming needs weight board for `16.2` **Y**
- Career development sync check-ins  **Y**

### 2023-05-15 to 2023-05-19
- Finalizing deliverables for `16.1` **Y**
- Finalizing dev on-call schedule for June **Y**
- Reviewing hackathon ideas list and discussing with team **Y**

### 2023-05-08 to 2023-05-12
- `Needs weight` board maintenance to `16.2` **Y**
- Reviewing issues to be marked at risk for `16.0` **Y**
- Organize dev on-call procedures for June **N**

### 2023-05-01 to 2023-05-05
- Organizing issues for CI Catalog GA phase **Y**
- Beginning to organize dev on-call procedures for June **Y**
- Helping to prioritize issues for `16.1` **Y**

### 2023-04-24 to 2023-04-28
- Prepping needs weight board for May **Y**
- Gathering feedback from Assignment 3 syncs with customers for CI Catalog **Y**
- Finish Q2 OKR planning and Q1 OKR closeout **Y**

### 2023-04-17 to 2023-04-21
- Identifying showcase candidates to discuss with team members **Y**
- Reviewing Assignment 3 feedback for CI Catalog (due date 2023-04-24) and creating follow-up issues **Y**
- Reviewing Say/Do ratios and MR rates for correlation patterns for Q1 OKR **Y**
- Q2 OKR planning for team **Y**

### 2023-04-10 to 2023-04-14
- Marking issues at risk for carryover to `16.0` **Y**
- Merging remaining release posts for `15.11` **Y**
- Check in for JWT issue status updates **Y**
- Continue to plan for `16.0` focus **Y**

### 2023-04-03 to 2023-04-07
- Helping support the FCL and day-to-day questions in Pipeline Security **Y**
- Follow ups on 16.0 deprecation messaging for Lint endpoint **Y**
- Reviewing needs weight progress for team and next release MRs for release post inclusion **Y**
- Holiday 2023-04-07

### 2023-03-27 to 2023-03-31 
- PTO 2023-03-27 through 2023-03-30 (AM)

### 2023-03-20 to 2023-03-23
- Creating associated metrics collection issues for tracking usage of CI Catalog features **Y**
- Follow-up with Pipeline Security on newly inherited `16.0` breaking changes **Y**
- Reviewing issues to be prioritized for weighting in `16.0` for CI Catalog **Y**
- PTO 2023-03-24 through 2023-03-30 (AM)

### 2023-03-13 to 2023-03-17
- Applying deliverable labels in `15.11` planning [issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/89) **Y**
- Organizing issues for priority in CI Catalog [epic](https://gitlab.com/groups/gitlab-org/-/epics/7462) and `breaking changes` [issues](https://gitlab.com/groups/gitlab-org/-/boards/2019514?label_name[]=group%3A%3Apipeline%20authoring&label_name[]=breaking%20change&milestone_title=16.0) in `15.11` **Y**
- Merging release posts for `15.10` **Y**

### 2023-03-06 to 2023-03-10 
- Organizing next steps for priorities on FE for CI Catalog in https://gitlab.com/groups/gitlab-org/-/epics/9957 **Y**
- Discuss Assignment 3 CI Catalog Alpha Program direction **Y**
- `15.11` planning based on milestone carryover and goals **Y**

### 2023-02-28 to 2023-03-03 
- Finalizing monthly weighting week [board](https://gitlab.com/groups/gitlab-org/-/boards/2019514?label_name[]=group%3A%3Apipeline%20authoring&label_name[]=workflow%3A%3Aplanning%20breakdown&label_name[]=candidate%3A%3A15.11) for `15.11` **Y**
- Organizing CI Catalog [epic](https://gitlab.com/groups/gitlab-org/-/epics/7462) for upcoming milestone work **Y**
- Initial kickoff career development quarterly syncs with new team members **Y**
- Reviewing CI Catalog participant feedback from `Include` syntax in Dovetail **Y**

### 2023-02-20 to 2023-02-24 
- Reviewing sev2 frontend [board](https://gitlab.com/groups/gitlab-org/-/boards/2019514?label_name[]=group%3A%3Apipeline%20authoring&label_name[]=frontend&label_name[]=severity%3A%3A2) for potential `15.10` prioritization **Y**
- Hydrating onboarding [board](https://gitlab.com/groups/gitlab-org/-/boards/2019514?label_name[]=group%3A%3Apipeline%20authoring&label_name[]=onboarding) with candidate issues for Sam and Kasia to start working on in `15.10` **Y**
- Reviewing error budget 7d [trends](https://dashboards.gitlab.net/d/stage-groups-detail-pipeline_authoring/stage-groups-pipeline-authoring-group-error-budget-detail?orgId=1&from=now-7d%2Fm&to=now%2Fm) and creating follow-up issues **Y**

### 2023-02-13 to 2023-02-17 
- Fix Sisense calculations for PA MR rates for Q1 OKR **Y**
- Organizing `15.10` deliverables for CI Catalog **Y**
- Reviewing error budget and creating any further issues for investigation **Y**
- Prepping `15.9` closeout and anticipated rollover planning **Y**

### 2023-02-06 to 2023-02-10
- Deliver Annual FY'23 Compensation Reviews to team. **Y**
- Review unweighted issues to continue to next month's weighting week. **Y**
- Organizing issues for next milestone for CI Components Catalog. **Y**

### 2023-01-30 to 2023-02-03
- Prepping monthly weighting week [board](https://gitlab.com/groups/gitlab-org/-/boards/2019514?label_name[]=group%3A%3Apipeline%20authoring&label_name[]=workflow%3A%3Aplanning%20breakdown&label_name[]=candidate%3A%3A15.10) for `15.10`  **Y**
- Follow-up with scalability on recent error budget [degradation](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2139)  **Y**
- Review pilot [sessions](https://gitlab.dovetailapp.com/projects/1b0zYNc1CaxzyVqnRuGU5a/readme) for Pipeline Authoring tasks.  **Y**

### 2023-01-23 to 2023-01-27
- Adjust Say/Do report to be deliverable-based in future milestones. **Y**
- Managing customer escalation issue to completion and updating RCA. **Y**
- Working on promotion planning doc review and feedback. **Y**
- Reviewing GitLab CI Workflows epic/issues for next steps. **Y**
- Updating CI Components/Catalog project for next issues. **Y**

### 2023-01-18 to 2023-01-20
- Updating CI Components/Catalog project with issues and milestones **Y**
- Complete next steps in FY'23 Equity/Compensation cycle **Y**
- Creating new spreadsheet to manage deliverable Say/Do in milestones **Y**

### 2023-01-09 to 2023-01-13
- Finishing sharing FY'23 performance ratings and feedback with team **Y**
- 15.9 issue planning and 15.8 release post merging **Y**
- Attending Quality Engineer Bi-Weekly meeting to gather feedback for CI Components/Catalog project **Y**
- Needs weight monthly rollover tasks **Y**
- Reviewing 15.9/15.10 CI Components/Catalog issue candidates **Y**

### 2023-01-02 to 2023-01-06
- Setting up yearly reviews with team members **Y**
- Reviewing Monthly weighting issues week **Y**
- Reviewing CI Components and CI Catalog project and updating epic with issues/due dates **Y**
- Action items from CI Components and CI Catalog weekly kickoff **Y**

### 2022-12-19 to 2022-12-23
- Complete issue creation for deliverables in CI Components and CI Catalog project **Y**
- Reviewing team member career growth planning docs **Y**
- 15.7->15.8 carry over tasks **Y**
- Updating gap analysis [issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/14293) for PA **Y**
- I will be on PTO after 2022-12-23 until 2023-01-03

### 2022-12-12 to 2022-12-16 

- Reviewing CI Catalog project timelines with team members and firming up next steps for deliverables **N**
- Finalizing performance ratings for team members in Workday **Y**
- Merging 15.7 release posts **Y**
- Reviewing and sharing Sisense Milestone Planning **Y**

### 2022-12-05 to 2022-12-09 

- Updating Milestone Planning dashboard in Sisense for accuracy on reporting **Y**
- Adding issues to 15.7 bugs/performance/usability release MRs **Y**
- Updating 15.7 prioritization for team members **Y**
- I will be on PTO from 2022-12-06 through 2022-12-09

### 2022-11-28 to 2022-12-02
- Finalizing achievements/strengths/areas of improvement for Talent Asssessments **Y**
- Updating our handbook to reference `workflow::planning breakdown` for weighting issues **Y**
- Finalizing board of issues that need weight for first week of month **Y**
- Reviewing CI Catalog proposals/discussions **Y**

### 2022-11-21 to 2022-11-23

- Prepping for CI Components Catalog AMA on 2022-11-23 **Y** (Reviewed async video)
- Reviewing discussion/takeaways from Secrets Management Think Big recording on 2022-11-17 **Y**
- Reviewing Mean Time To Merge (MTTM) to identify areas to improve **Y**
- Finish achievements/strengths/areas of improvement for Talent Asssessments **N**
- 2022-11-24 and 2022-11-25 - Thanksgiving holiday (US)

### 2022-11-14 to 2022-11-18

- Complete planning for 15.7 milestone **Y**
- Merging all 15.6 release posts **Y**
- Finish achievements/strengths/areas of improvement for Talent Asssessments **N**
- Reviewing Error Budget affected areas and creating applicable issues for upcoming milestones. **Y**
- 2022-11-18 - Family & Friends Day

### 2022-11-07 to 2022-11-11

- Reviewing Secrets Features Feedback Survey **Y**
- Attach stage level OKRs to group OKRs **Y**
- Onboarding with Leaminn **Y**
- Reprioritizing 15.6 issues to accommodate error budget investigation **Y**
- Note: PTO on 2022-11-10 and 2022-11-11

### 2022-10-31 to 2022-11-04

- Complete Talent Assessments by 2022-11-04 **Y** - need to finish achievements/strengths/areas of improvement
- Synching to update statuses on top 15.6 priorities **Y**
- Reviewing error budgets **Y**
- Product Designer interview/feedback **Y**
- FEDRAMP API documentation status **Y**
- Onboarding with Leaminn **Y**

### 2022-10-24 to 2022-10-28

- Onboard new backend engineer team member to Pipeline Authoring **Y**
- Talent Assessments - my reports and my own **Y**
- Prep for Verify calibration **Y**
- Managing health statuses for milestone issues in PM Coverage issue **Y**
